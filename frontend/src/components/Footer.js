import React from 'react';
const Footer = () => {
 
    return(
        <div className='footer'>
            ©{localStorage.getItem('ro') === 'true' ? 'Proiect' : 'Project'}
            <br></br>
            mail: {localStorage.getItem('ro') === 'true' ? 'proiect@foarte.bun' : 'project@very.good'}
            <br></br>
            {localStorage.getItem('ro') === 'true' ? 'Nr. Tel.' : 'Phone Nr.'}: 0712345678
        </div>
    )
}
export default Footer