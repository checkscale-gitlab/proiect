import React from 'react';
import Header from './Header'
import Footer from './Footer'
import Autentificare from './Autentificare'
import Leaderboard from './Leaderboard';
import GameLogic from './game/GameLogic';
import Flagged from './flags/Flagged';
import FlaggedUsers from './FlaggedUsers';


import {ReactComponent as ReactLogo} from '../Tree_swing.svg';
const Layout = (props) => {

    // const [base, setBase] = useState('authors')
    const action = props.action
    const logHandler = props.logHandler

    return(
        <div className='layout'>
            <Header action={action} logHandler={logHandler} displayButtons = {action !== 'log' && action !== 'reg'}/>
            {props.children}
            {action === 'log' &&
                <div className='layoutcontent'>
                    <ReactLogo id='Image'/>
                    <Autentificare action='log' logHandler={logHandler}/>
                </div>
            }
            {action === 'reg' &&
                <div className='layoutcontent'>
                    <ReactLogo id='Image'/>
                    <Autentificare action='reg'/>
                </div>
            }
            {action === 'quiz' &&
                <div className='layoutcontent'>
                    <ReactLogo id='Image'/>
                </div>
            }
            {action === 'game' &&
                <div className='layoutcontent'>
                    <GameLogic />
                </div>
            }
            {action === 'leaderboard' &&
                <div className='layoutcontent'>
                    <Leaderboard />
                </div>
            }
            {action === 'flagged' &&
                <div className='layoutcontent'>
                    <Flagged />
                </div>
            }
            {action === 'users' &&
                <div className='layoutcontent'>
                    <FlaggedUsers />
                </div>
            }
            <Footer/>
        </div>
    )
}
export default Layout