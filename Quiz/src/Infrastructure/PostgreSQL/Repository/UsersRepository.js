const {
    queryAsync
} = require('..');

const getAllAsync = async() => {
    console.info ('Getting all users from database async...');
    
    return await queryAsync('SELECT id, username, role_id FROM users');
};

const getFirstNAsync = async () => {
    const nr_players = 10;
    console.info(`Getting first ${nr_players} players from database async...`);

    return await queryAsync('SELECT username, score \
                            FROM users \
                            ORDER BY score DESC, id \
                            LIMIT $1', [nr_players]);
};

const getFirstFlaggedNAsync = async () => {
    const nr_players = 10;
    console.info(`Getting first ${nr_players} players from database async...`);

    return await queryAsync('SELECT id, username, score, flags_score \
                            FROM users \
                            ORDER BY flags_score, score, id \
                            LIMIT $1', [nr_players]);
};

const addAsync = async (username, mail, password) => {
    console.info(`Adding user ${username} in database async...`);

    const regularUserId = 1;
    const users = await queryAsync('INSERT INTO users (username, mail, password, role_id) VALUES ($1, $2, $3, $4) RETURNING id, username, role_id', [username, mail, password, regularUserId]);
    return users[0];
};

const getByUsernameWithRoleAsync = async (username) => {
    console.info(`Getting user with username ${username} from database async...`);
    
    const users = await queryAsync(`SELECT u.id, u.password, u.mail_confirmed,
                                u.username, r.value as role,
                                r.id as role_id FROM users u 
                                JOIN roles r ON r.id = u.role_id
                                WHERE u.username = $1`, [username]);
    return users[0];
};

const getByUserIdAsync = async (user_id) => {
    console.info(`Getting user with id=${user_id} from database async...`);
    
    const users = await queryAsync(`SELECT u.id, u.password, u.mail_confirmed,
                                u.username, r.value as role,
                                r.id as role_id FROM users u 
                                JOIN roles r ON r.id = u.role_id
                                WHERE u.id = $1`, [user_id]);
    return users[0];
};

const updateRoleByIdAsync = async (user_id, role_id) => {
    console.info(`Updating role_id ${role_id} for id=${user_id} from database async...`);

    const users = await queryAsync('UPDATE users SET role_id = $1 WHERE id = $2 RETURNING *', [role_id, user_id]);
    return users[0];
};

const updateConfirmedMailAsync = async (user_id) => {
    console.info(`Updating confirmed_mail for for id=${user_id} from database async...`);

    const users = await queryAsync('UPDATE users SET confirmed_mail = TRUE WHERE id = $1 RETURNING *', [user_id]);
    return users[0];
};

const deleteByNrFlagsAsync = async (flags_score) => {
    console.info(`Deleting users with flags_score < ${flags_score} from database async...`);

    return await queryAsync('DELETE FROM users U WHERE U.flags_score < $1 RETURNING *', [flags_score]);
}

const addScoreByIdAsync = async (user_id, score) => {
    console.info(`Adding ${score} score for user with id=${user_id} from database async...`);

    const users = await queryAsync('UPDATE users SET score = score + $1 WHERE id = $2 RETURNING *', [score, user_id]);
    return users[0].score;
};

module.exports = {
    addScoreByIdAsync,
    getAllAsync,
    addAsync,
    getByUsernameWithRoleAsync,
    getByUserIdAsync,
    updateRoleByIdAsync,
    deleteByNrFlagsAsync,
    getFirstNAsync,
    getFirstFlaggedNAsync,
    updateConfirmedMailAsync
}