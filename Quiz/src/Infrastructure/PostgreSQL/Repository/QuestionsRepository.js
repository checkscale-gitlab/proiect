const {
    queryAsync
} = require('..');

const getAllAsync = async () => {
    console.info(`Getting all questions from database async...`);

    return await queryAsync('SELECT Q.id as "question_id", Q.full_statement as "question_statement", Q.a0 as "right_first_answer", Q.a1 as "second_answer", \
                                    Q.a2 as "third_answer", Q.a3 as "fourth_answer", Q.domain as "question_domain", Q.correct as "correct" \
                            FROM questions Q');
};

const getByIdAsync = async (id) => {
    console.info(`Getting the question with id=${id} from database async...`);

    return await queryAsync('SELECT Q.id as "question_id", Q.full_statement as "question_statement", Q.a0 as "right_first_answer", Q.a1 as "second_answer", \
                                    Q.a2 as "third_answer", Q.a3 as "fourth_answer", Q.domain as "question_domain" Q.correct as "correct" \
                            FROM questions Q \
                            WHERE Q.id = $1', [id]);
};

const getRandomNAsync = async (nr_questions) => {
    console.info(`Getting ${nr_questions} random questions from database async...`);

    return await queryAsync('SELECT Q.id as "question_id", Q.full_statement as "question_statement", Q.a0 as "right_first_answer", Q.a1 as "second_answer", \
                                    Q.a2 as "third_answer", Q.a3 as "fourth_answer", Q.domain as "question_domain" \
                            FROM questions Q \
                            ORDER BY RANDOM() \
                            LIMIT $1', [nr_questions]);
};


const getRandomNByDomain = async (nr_questions, domain) => {
    console.info(`Getting ${nr_questions} random questions in domain ${domain} from database async...`);

    return await queryAsync('SELECT Q.id as "question_id", Q.full_statement as "question_statement", Q.a0 as "right_first_answer", Q.a1 as "second_answer", \
                                    Q.a2 as "third_answer", Q.a3 as "fourth_answer" \
                            FROM questions Q WHERE Q.domain=($2)\
                            ORDER BY RANDOM() \
                            LIMIT $1', [nr_questions, domain]);
};


const addAsync = async (full_statement, a0, a1, a2, a3, domain, correct) => {
    console.info(`Adding question in database async...`);

    const questions = await queryAsync('INSERT INTO questions (full_statement, a0, a1, a2, a3, domain, correct) VALUES ($1, $2, $3, $4, $5, $6, $7) RETURNING *',
                                        [full_statement, a0, a1, a2, a3, domain, correct]);
    return questions[0];
};

const addNoDomainAsync = async (full_statement, a0, a1, a2, a3, correct) => {
    console.info(`Adding question in database async...`);

    const questions = await queryAsync('INSERT INTO questions (full_statement, a0, a1, a2, a3, correct) VALUES ($1, $2, $3, $4, $5, $6) RETURNING *', [full_statement, a0, a1, a2, a3, correct]);
    return questions[0];
};

const updateByIdAsync = async (id, full_statement, a0, a1, a2, a3, domain, correct) => {
    console.info(`Updating the question with id=${id} from database async...`);

    const questions = await queryAsync('UPDATE questions \
                                        SET full_statement = $2, a0 = $3, a1 = $4, a2 = $5, a3 = $6, domain = $7, correct = $8 \
                                        WHERE id = $1 RETURNING *', [id, full_statement, a0, a1, a2, a3, domain, correct]);
    return questions[0];
};

const deleteByIdAsync = async (id) => {
    console.info(`Deleting the question with id=${id} from database async...`);

    const questions = await queryAsync('DELETE FROM questions WHERE id = $1 RETURNING *', [id]);
    return questions[0];
};

const checkAnswers = async (answers) => {
    console.info(`Checking answers from database async...`);
    let score = 0;
    let pairs = [];

    for (let index = 0; index < answers.length; index++) {
        const element = answers[index];
        let q = await queryAsync('SELECT Q.a0 as "a0", Q.a1 as "a1", Q.a2 as "a2", Q.a3 as "a3", Q.correct as "correct" \
                                FROM questions Q \
                                WHERE Q.id = $1', [element.question_id]);
        q = q[0];
        if (q.correct == element.answer) {
            score += 1;
        }
        let possible_answers = [q.a0, q.a1, q.a2, q.a3];
        pairs.push({chosen: possible_answers[parseInt(element.answer.slice(-1))], real: possible_answers[parseInt(q.correct.slice(-1))]});
    }
    console.log(pairs);
    return {score: score, pairs: pairs};
}

module.exports = {
    getAllAsync,
    getByIdAsync,
    getRandomNAsync,
    getRandomNByDomain,
    addAsync,
    addNoDomainAsync,
    updateByIdAsync,
    deleteByIdAsync,
    checkAnswers
}