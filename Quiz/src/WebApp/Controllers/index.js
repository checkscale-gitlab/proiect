const Router = require('express')();

/* Import controllers */
const UsersController = require('./UsersController.js');
const RolesController = require('./RolesController.js');
const QuestionsController = require('./QuestionsController.js');
const FlaggedQuestionsController = require('./FlaggedQuestionsController.js');
const MailController = require('../EmailConfirmationHandler');
const { authorizeAndExtractTokenAsync } = require('../Filters/JWTFilter.js');

/* Add controllers to main router */
Router.use('/users', UsersController);
Router.use('/roles', authorizeAndExtractTokenAsync, RolesController);
Router.use('/questions', authorizeAndExtractTokenAsync, QuestionsController);
Router.use('/flagged_questions', authorizeAndExtractTokenAsync, FlaggedQuestionsController);
Router.use('/mail', MailController);

module.exports = Router;