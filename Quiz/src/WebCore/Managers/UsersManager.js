const UsersRepository = require('../../Infrastructure/PostgreSQL/Repository/UsersRepository.js');
const AuthenticatedUserDto = require('../DTOs/AuthenticatedUserDto.js');
const RegisteredUserDto = require('../DTOs/RegisteredUserDto.js');
const JwtPayloadDto = require('../DTOs/JwtPayloadDto.js');
const ServerError = require('../../WebApp/Models/ServerError.js');

const { hashPassword, comparePlainTextToHashedPassword } = require('../Security/Password')
const { generateTokenAsync } = require('../Security/Jwt');

const authenticateAsync = async (username, plainTextPassword) => {

    console.info(`Authenticates user with username ${username}`);

    const user = await UsersRepository.getByUsernameWithRoleAsync(username);
    
    if (!user) {
        throw new ServerError(`Utilizatorul cu username ${username} nu exista in sistem!`, 404);
    }

    console.log('mail_confirmed:', user.mail_confirmed);
    if (!user.mail_confirmed) {
        throw new ServerError(`Utilizatorul cu username ${username} nu si-a confirmat adresa de mail!`, 404);
    }

    //const hashedPassword = hashPassword(plainTextPassword);
    const correct_password = await comparePlainTextToHashedPassword(plainTextPassword, user.password);
    if (!correct_password) {
        throw new ServerError(`Parola este gresita!`, 404);
    }
    const payload = new JwtPayloadDto(user.id, user.role);
    const token = await generateTokenAsync(payload);
    const authUserDto = new AuthenticatedUserDto(token, user.username, user.role);
    return authUserDto;
};

const registerAsync = async (username, mail, plainTextPassword) => {
    const hashedPassword = await hashPassword(plainTextPassword);
    const regUser = await UsersRepository.addAsync(username, mail, hashedPassword);
    const regUserDto = new RegisteredUserDto(regUser.id, username, regUser.role_id);
    return regUserDto;
};

module.exports = {
    authenticateAsync,
    registerAsync
}